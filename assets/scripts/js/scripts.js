/**
 * Created by sebci on 07.03.2017.
 */




$(document).ready(function(){


/**
    window.onload = function(){
        var input;
        input = document.getElementById('name').offsetWidth;
        document.getElementById('tresc').setAttribute("style","min-width: "+input+"px;");
    };
**/


    (function() {

        "use strict";

        var toggles = document.querySelectorAll('#menu-button, #mobile-menu a[href*="#"]:not([href="#"])');

        for (var i = toggles.length - 1; i >= 0; i--) {
            var toggle = toggles[i];
            toggleHandler(toggle);
        }

        function toggleHandler(toggle) {
            toggle.addEventListener( "click", function(e) {
                e.preventDefault();
                var mbttn = document.getElementById('menu-button');
                (mbttn.classList.contains("is-active") === true) ? mbttn.classList.remove("is-active") : mbttn.classList.add("is-active");
            });
        }

    })();

        $('#menu-button, #mobile-menu a[href*="#"]:not([href="#"])').click(function(){
            $("#mobile-menu").slideToggle("slow");
        });

//Smooth scroll on click
    $(function() {

        $('a[href*="#"]:not([href="#"])').click(function () {

            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                && location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

                if (target.length) {
                    $('html, body').animate({
                        'scrollTop': target.offset().top
                    }, 1000, 'swing');
                    return false;
                }
            }
        });
    });


// Get the modal
    var kont_modal = document.getElementById('kontakt_modal');
    var bizn_modal = document.getElementById('biznes_modal');
    var wspol_modal = document.getElementById('wspolpr_modal');
    var graf_modal = document.getElementById('grafika_modal');
    var strony_modal = document.getElementById('www_modal');
// Get the button that opens the modal
    var kon_btn1 = document.getElementById("kontakt_Btn1");
    var kon_btn2 = document.getElementById("kontakt_Btn2");
    var biznBtn = document.getElementById('biznesBtn');
    var wspolBtn = document.getElementById('wspolprBtn');
    var grafBtn = document.getElementById('grafikaBtn');
    var stronyBtn = document.getElementById('wwwBtn');

// Get the <span> element that closes the modal
    var span_kont = document.getElementsByClassName("close")[0];
    var span_bizn = document.getElementsByClassName("close")[1];
    var span_wspol = document.getElementsByClassName("close")[2];
    var span_graf = document.getElementsByClassName("close")[3];
    var span_strony = document.getElementsByClassName("close")[4];

// When the user clicks on the button, open the modal

        kon_btn1.onclick = function () {
        kont_modal.style.display = "flex";
    };
    kon_btn2.onclick = function () {
        kont_modal.style.display = "flex";
    };
    biznBtn.onclick = function () {
        bizn_modal.style.display = "flex";
    };
    wspolBtn.onclick = function () {
        wspol_modal.style.display = "flex";
    };
    grafBtn.onclick = function () {
        graf_modal.style.display = "flex";
    };
    stronyBtn.onclick = function () {
        strony_modal.style.display = "flex";
    };


// When the user clicks on <span> (x), close the modal
    span_kont.onclick = function () {
        kont_modal.style.display = "none";
    };
    span_bizn.onclick = function () {
        bizn_modal.style.display = "none";
    };
    span_wspol.onclick = function () {
        wspol_modal.style.display = "none";
    };
    span_graf.onclick = function () {
        graf_modal.style.display = "none";
    };
    span_strony.onclick = function () {
        strony_modal.style.display = "none";
    };

// When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == kont_modal ||
            event.target == bizn_modal ||
            event.target == wspol_modal ||
            event.target == graf_modal ||
            event.target == strony_modal) {
            kont_modal.style.display = "none";
            bizn_modal.style.display = "none";
            wspol_modal.style.display = "none";
            graf_modal.style.display = "none";
            strony_modal.style.display = "none";        }
    };





});


