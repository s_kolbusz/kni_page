/**
 * Created by sebci on 29.03.2017.
 */

function validateEmail() {

    var emailID = document.contact.email.value;
    var atpos = emailID.indexOf("@");
    var dotpos = emailID.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=emailID.length)
    {
        return false;
    }
    return (true);
}

function correctCaptcha() {
    var btnSubmit = document.getElementById("send_form");

    if ( btnSubmit.classList.contains("hidden") ) {
        btnSubmit.classList.remove("hidden");
        btnSubmit.classList.add("block");
    }
}


function form_validate() {

    var name = document.contact.name;
    var email_from = document.contact.email;
    var subject = document.contact.temat;
    var message = document.contact.tresc;


    if (name.value == "" || name.value == null || typeof name.value == 'undefined') {
        alert('Field "Name and last name" is required');
        name.focus();
        return false;
    } else if (name.value.length < 3) {
        alert('Your name must be real name');
        name.focus();
        return false;
    }

    if (email_from.value == "" || email_from.value == null || typeof email_from.value == 'undefined') {
        alert('Field "E-mail" is required');
        email_from.focus();
        return false;
    } else if (email_from.value.length < 4 || !validateEmail() ) {
        alert('This e-mail address is incorrect');
        email_from.focus();
        return false;
    }

    if((subject.value == "" || subject.value == null || typeof subject.value == 'undefined')
        && (message.value == "" || message.value == null || typeof message.value == 'undefined')){
        subject.value = "No subject";
        message.value = "Field message was empty";
        return false;
    }

    if (subject.value == "" || subject.value == null || typeof subject.value == 'undefined') {
        subject.value = "No subject";
        return false;
    }

    if (message.value == "" || message.value == null || typeof message.value == 'undefined') {
        message.value = "Field message was empty";
        return false;
    }


    return ( true );

}




function form_send(){
    if (form_validate()){
        $('#contact').submit(function (e) {
            $.ajax({
                type: 'POST',
                url: '../assets/scripts/send_mail.php',
                data: $(this).serialize(),
                success: function (data) {
                    alert(data);
                }
            });
            return false;
        });
    }
    else{
        alert('Fields "Subject" and "Message" was empty, so now form will be sent with default values.' +
            ' If you agree, send now by click "SEND" button.');
        return false;
    }
}