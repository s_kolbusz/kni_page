/**
 * Created by sebci on 29.03.2017.
 */

function validateEmail() {

    var emailID = document.contact.email.value;
    var atpos = emailID.indexOf("@");
    var dotpos = emailID.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=emailID.length)
    {
        return false;
    }
    return (true);
}

function correctCaptcha() {
    var btnSubmit = document.getElementById("send_form");

    if ( btnSubmit.classList.contains("hidden") ) {
        btnSubmit.classList.remove("hidden");
        btnSubmit.classList.add("block");
    }
}


function form_validate() {

    var name = document.contact.name;
    var email_from = document.contact.email;
    var subject = document.contact.temat;
    var message = document.contact.tresc;


    if (name.value == "" || name.value == null || typeof name.value == 'undefined') {
        alert('Pole "Imię i nazwisko" jest wymagane');
        name.focus();
        return false;
    } else if (name.value.length < 3) {
        alert('Prosimy podać prawdziwe imię oraz nazwisko');
        name.focus();
        return false;
    }

    if (email_from.value == "" || email_from.value == null || typeof email_from.value == 'undefined') {
        alert('Pole "Adres e-mail" jest wymagane');
        email_from.focus();
        return false;
    } else if (email_from.value.length < 4 || !validateEmail() ) {
        alert('Podany adres e-mail jest nieprawidłowy');
        email_from.focus();
        return false;
    }

    if((subject.value == "" || subject.value == null || typeof subject.value == 'undefined')
        && (message.value == "" || message.value == null || typeof message.value == 'undefined')){
        subject.value = "Brak tematu";
        message.value = "Osoba nie podała treści wiadomości";
        return false;
    }

    if (subject.value == "" || subject.value == null || typeof subject.value == 'undefined') {
        subject.value = "Brak tematu";
        return false;
    }

    if (message.value == "" || message.value == null || typeof message.value == 'undefined') {
        message.value = "Osoba nie podała treści wiadomości";
        return false;
    }


    return ( true );

}




function form_send(){
    if (form_validate()){
        $('#contact').submit(function (e) {
            $.ajax({
                type: 'POST',
                url: 'assets/scripts/send_mail.php',
                data: $(this).serialize(),
                success: function (data) {
                    alert(data);
                }
            });
            return false;
        });
    }
    else{
        alert('Pole "Temat" oraz "Treść wiadomośći" zostały automatycznie uzupełnione. ' +
            'Po wciśnięciu przycisku "wyślij" formularz zostanie wysłany z domyślnymi wartościami.');
        return false;
    }
}