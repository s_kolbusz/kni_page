<?php

    $language = $_POST['language'];

    if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {

        //your site secret key
        $secret = '6LfUhBoUAAAAABNAm_Y2UPgF9XPNlKjgTGVshSoO';
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if ($responseData->success) {

            if (($_POST['name'] != null || $_POST['name'] != "")
                || ($_POST['mail'] != null || $_POST['mail'] != "")
            ) {

                $email_to = "kni.uek@gmail.com";
                $email_from = $_POST['email'];
                $name = $_POST['name'];
                $subject = $_POST['temat'];


                $email_message = "Została do Ciebie wysłana wiadomość: \n";

                function clean_string($string)
                {
                    $bad = array("content-type", "bcc:", "to:", "cc:", "href");
                    return str_replace($bad, "", $string);
                }

                $email_message .= "Od: " . clean_string($name) . ", " . clean_string($email_from) . "\n";
                $email_message .= "-----------------------------------------------------------\n";
                $email_message .= "Treść: \n" . clean_string($_POST['tresc']) . "\n";

                $headers = 'From: ' . $email_from . "\r\n" .
                    'Reply-To: ' . $email_to . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();

                mail($email_to, $subject, $email_message, $headers);

                if ($language === "en") {
                    echo 'Message send.';
                }
                elseif ($language === "pl"){
                    echo 'Wiadomość wysłana!';
                }
            } else {
                echo 'error';
            }
        } else {
            if ($language === "en") {
                echo 'Validation failed, proof you\'re human.';
            }
            elseif ($language === "pl"){
                echo 'Weryfikacja się nie powiodła. Udowodnij że nie jesteś robotem.';
            }
        }
    } else {
        if ($language === "en"){
                echo 'Validation failed, proof you\'re human.';
        }
        elseif ($language === "pl") {
                echo "Weryfikacja się nie powiodła. Udowodnij że nie jesteś robotem.";
            }
    }
?>